#!/usr/bin/php
<?php
require_once 'vendor/autoload.php';

use GorkaLaucirica\HipchatAPIv2Client\Auth\OAuth2;
use GorkaLaucirica\HipchatAPIv2Client\Client;
use GorkaLaucirica\HipchatAPIv2Client\API\RoomAPI;
use GorkaLaucirica\HipchatAPIv2Client\Model\Message;

// Variables.
$m_time = microtime();
$m_time = explode(" ", $m_time);
$t_start = $m_time[1] + $m_time[0];
$tokenToggl = 'a9f2abde1194277cb1fbf343297f8816';
$hipChatToken = 'ZkErkoWQfJ3UCyLybpfvCZ5h8nwm1nGAipiW0aCJ';
$roomId = 2161255;
$roomId = 2382263; // My test room.

/**
 * Convert string to underscored code standard.
 * @param $str
 * @param array $noStrip
 * @return mixed|string
 */
function underscore($str, array $noStrip = []) {
  $str = preg_replace('/[^a-z0-9' . implode("", $noStrip) . ']+/i', ' ', $str);
  $str = trim($str);
  $str = str_replace(" ", "_", $str);
  $str = strtolower($str);
  return $str;
}

/**
 * Get toggl data.
 */
function get_toggl_data($url, $token, $timeout = 5) {
  header('Content-type: application/json');
  $ch = curl_init();
  curl_setopt($ch, CURLOPT_URL, $url);
  curl_setopt($ch, CURLOPT_HTTPGET, TRUE);
  curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
  curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, $timeout);
  curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
  curl_setopt($ch, CURLOPT_USERPWD, $token . ':api_token');
  $data = curl_exec($ch);
  curl_close($ch);
  return $data;
}

$start_date = date("Y-m-d") . 'T06:00:00+02:00';
$end_date = date("Y-m-d") . 'T23:59:00+02:00';

$returnedData = get_toggl_data("https://www.toggl.com/api/v8/time_entries?start_date=" . urlencode($start_date) . "&end_date=" . urlencode($end_date), $tokenToggl);
$data = json_decode($returnedData, TRUE);

$output = '';
$mapping = [];
$result = [];
foreach ($data as $key => $object) {
  $data[$key]['machine_name'] = underscore($object['description']);
}
foreach ($data as $key => $object) {
  if (isset($object['stop'])) {
    $start = strtotime($object['start']);
    $end = strtotime($object['stop']);
    $time_diff = $end - $start;
    $mapping[$key] = underscore($object['machine_name']);
    if (array_search($object['machine_name'], $mapping)) {
      $result[$key]['time'] = $time_diff;
      $result[$key]['description'] = $object['description'];
    }
    else {
      $result[array_search($object['machine_name'], $mapping)]['time'] += $time_diff;
      $result[array_search($object['machine_name'], $mapping)]['description'] = $object['description'];
    }
  }
}
$total = 0;
foreach ($result as $res) {
  $output .= $res['description'] . ' - ' . '<b>' . gmdate('H:i:s', $res['time']) . '</b><br>';
  $total += $res['time'];
}
//Send message.
if (!empty($output)) {
  $output .= '<br><span style="color:#610B0B; font-weight:bold;">Total: </span><b>' . gmdate('H:i:s', $total) . '</b><br>';
  $auth = new OAuth2($hipChatToken);
  $client = new Client($auth);
  $roomAPI = new RoomAPI($client);
  $json = [
    'id' => NULL,
    'from' => '',
    'message' => $output,
    'color' => 'green',
    'notify' => TRUE,
    'messageFormat' => 'html',
    'date' => NULL,
  ];
  $message = new Message($json);
  $roomAPI->sendRoomNotification($roomId, $message);
}
$m_time = microtime();
$m_time = explode(" ", $m_time);
$m_time = $m_time[1] + $m_time[0];
$total_time = ($m_time - $t_start);
printf("Execution time %f sec.", $total_time);
echo "
";